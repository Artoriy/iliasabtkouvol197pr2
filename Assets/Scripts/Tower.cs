﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {

    [SerializeField]
    private Transform Cannon;
    [SerializeField]
    private GameObject Bullet;
    [SerializeField]
    private Transform SpawnPoint;

    private bool isRotating = false;

    private float firstTimer = 6.0f;
    private float time = 0.5f;
    private float randomDirection;
    private float endDirection;
    private int maxShots = 12;
    private int currentShots = 0;
    public int SetCurrentShots
    {
        set { currentShots = value; }
    }

    private Color32 activeColor = new Color(255,0,0);
    private Color32 diActiveColor = new Color(255, 255, 255);

    private void Start()
    {
        MainUI.mainUI.AddScore();
    }

    private void Update()
    {
        firstTimer -= Time.deltaTime;

        if (firstTimer <= 0 && currentShots < maxShots) // Cannon is active
        {
            Timer();
            Rot();
            Cannon.GetComponent<SpriteRenderer>().color = activeColor;
            GetComponent<SpriteRenderer>().color = activeColor;
        }
        else if (currentShots >= maxShots) // Cannon is diactive
        {
            Cannon.GetComponent<SpriteRenderer>().color = diActiveColor;
            GetComponent<SpriteRenderer>().color = diActiveColor;
        }
    }

    private void Timer()
    {
        time -= Time.deltaTime;
        if(time <= 0 && !isRotating)
        {
            isRotating = true;
            randomDirection = Random.Range(15, 45);

            if (Cannon.eulerAngles.z + randomDirection <= 360)
                endDirection = Cannon.eulerAngles.z + randomDirection;
            else
            {
                float newPos = Cannon.eulerAngles.z + randomDirection - 360;
                endDirection = newPos;
            }
        }
    }

    private void Rot()
    {
        if (isRotating)
        {
            Cannon.RotateAround(transform.position, Vector3.forward, 100 * Time.deltaTime);

            if(Cannon.eulerAngles.z >= endDirection)
            {
                isRotating = false;
                time = 0.5f;
                GameObject g = Instantiate(Bullet, SpawnPoint.position, Quaternion.identity);
                g.GetComponent<Bullet>().BulletDir = SpawnPoint.up;
                currentShots += 1;
            }
        }
    }

}
