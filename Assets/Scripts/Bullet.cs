﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    [SerializeField]
    private GameObject Tower;

    private float speed = 4;
    private float maxDistance;
    private float currentDistance;
    private Vector2 startPosition;
    private Vector2 currentPosition;

    private Vector2 bulletDirection;
    public Vector2 BulletDir
    {
        set { bulletDirection = value; }
    }

    private void Awake()
    {
        maxDistance = Random.Range(1, 4);
        startPosition = transform.position;
    }

	private void Update()
    {
        Movement();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.layer == 8)
        {
            MainUI.mainUI.LoseScore();
            Destroy(col.gameObject);
            Destroy(gameObject);
        }
    }

    private void Movement()
    {
        transform.Translate(bulletDirection * speed * Time.deltaTime);
        currentPosition = transform.position;
        currentDistance = Vector2.Distance(startPosition, currentPosition);

        if(currentDistance >= maxDistance)
        {
            if (MainUI.mainUI.isSpawning)
                Instantiate(Tower, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }

}
