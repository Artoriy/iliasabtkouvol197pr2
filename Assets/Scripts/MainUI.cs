﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainUI : MonoBehaviour {

	public static MainUI mainUI;

	[SerializeField]
	private Text towerScoreText;

	private int towerScore = 0;
	internal bool isSpawning = true;

	private void Awake()
    {
		mainUI = this;
    }

	internal void AddScore()
    {
		towerScore += 1;
		towerScoreText.text = "Towers: " + towerScore.ToString();

		StopSpawning();
	}

	internal void LoseScore()
	{
		towerScore -= 1;
		towerScoreText.text = "Towers: " + towerScore.ToString();

		StopSpawning();
	}

	private void StopSpawning()
    {
		if (towerScore >= 100)
		{
			isSpawning = false;

			Tower[] allTowers = FindObjectsOfType<Tower>();
			foreach (Tower tower in allTowers)
			{
				tower.SetCurrentShots = 0;
			}
		}
	}

}
